---
title: Analysis of Accidents and Traffic in London
author: Hamza Niazi, Rahul Kadri, Kunj Kinger, Z Shetty Manisha Nagesh, Syed Muhammad Omer
subtitle:  Group 16
date: "December 2019"
---

**Abstract**. **Context**- There are millions of lives lost every year in road accidents all around the world.

**Problem:** What are the factors that contribute to road accidents and what has been the trend of the rate of accidents?

**Method:** We have performed a statistical analysis of a dataset obtained from Kaggle which contained data about traffic flow and traffic accidents for the years 2000-2016 of the UK. For our analysis, we filtered out the data for the city of London and the years 2012 to 2014. In this paper, we are trying to determine whether the rate of accidents is more in urban or developed areas as compared to rural areas. Also as a secondary question, we are looking at the weather and light circumstances under which most accidents take place.

**Results:** The statistical and graphical analysis of the dataset revealed that Urban areas are the ones with a higher number of accidents. As for the second question that was looking for weather and light circumstances, the results yielded that a majority of the accidents took place under " Dry " weather conditions in broad daylight.

**Implications:** We conclude that urban areas with developed infrastructures still experience a higher number of road accidents. Higher Traffic density could also be a significant contributing factor to these accident rates. Also over the years 2012-2014, as per our analysis, the majority of the accidents happened under dry weather conditions with ample of light.


# 1. Introduction

London is Britain's largest metropolis and one of the most cosmopolitan. It is noticed that in the region of London traffic accidents take place on a large scale. the main reason behind this road accident is weather and light conditions, in consideration of the time and period of the year. “vehicle collisions cause more than 1.2 million deaths worldwide and an even greater number of non-fatal injuries each year”. (J. Rolison, et al., 2018). The relationship between traffic accidents and weather is not easy, nor is it evident. For example, the number of accidents increases in the wet weather (Edwards, 1998) We have made the report regarding road accidents which consider the main reason like weather and light conditions. London does not have a particular climate. Climate differences in the number of accidents in fine weather, storms, high winds, fog and snow are discussed and a comparison is made around London between the frequency of injuries and the environment (Edwards, 1996). light and weather are the attributes that are extensively dangerous and many casualties occur. The crashes fatalities occur more to be in urban areas rather than rural in London.
Traffic congestion can also be a factor that contributes to the collision disasters and loss of lives. If the mass or excess traffic the fatalities or accidents will be reduced as every vehicle would reduce the speed and converse may also happen.  Congestion and density of traffic are not identical terms, and it is unknown how the degree of congestion in density progresses.T he research aims to investigate the effect of traffic congestion on road accident incidence using an approach to spatial analysis while accounting for other relevant factors that may influence road accidents. Traffic density has been found to have less effect on the rate of accidents and economic loss (Wang, Quddus and Ison, 2009).


# 2. Background
 
Traffic accidents are disasters and the loss of lives is unpredictable, accidents can be caused by various means like rough weather, poor light conditions, and even traffic density. While the weather may not be a big cause of traffic incidents, it is an important component. The weather is considered by most people as not deterrent to driving until conditions become so extreme (Edwards, 1996). Rough weather causes accidents by adding many physical effects that worsen the driving environment, including lack of traction between the tyre and the ground and reduced visibility by the windshield rain and spray from other vehicles (Jaroszweski and McNamara, 2014). Road users are faced with two problems which they fear the most, Traffic congestion and road accidents. People suffer losses in terms of economy and quality of life when they are subjected to higher travel time caused by traffic congection (Wang, Quddus and Ison, 2009)

**Road Disasters Caused by Poor Weather**
Winter and Rainfall are consistently cited as the weather type responsible for the greatest number of weather-related accidents (Edwards, 1998). It is argued that previous approaches to rainfall quantification for accident analysis, primarily using a representative surface meteorological station to represent an urban area (Edwards, 1996). As a result, there has been a growing research agenda around studies on urban areas across the sciences, including a particular focus on cities by the climate change impact community (Jaroszweski and McNamara, 2014). Although the modernized safety-critical vehicle design and intensive driving training have led to reductions in weather-related accident rates across the developed world in recent years. In a study, the effect of rainfall is usually expressed through relative accident rates (RARs) (Edwards, 1996). In this study, the rainfall period as compared to periods of dry weather using Radar-based matched pairs analysis. It has also been shown that greater rainfall intensities lead to higher RAR's and injury rates (Edwards, 1996) (Hambly, 2013). 

![image.png](./assets/image1.png)

(Jaroszweski and McNamara, 2014)
Map showing the 12 study site selection areas under consideration and the corresponding table showing a number of accidents reported in 2010 under rain conditions (selected red cities). One of the major problems in urban studies is the lack of reflective observations of the meteorological station at sufficient spatial and temporal resolutions (Jaroszweski and McNamara, 2014). The relation between poor weather and rate of road accidents was the main objective although the results were reliable because of the limited amount of data. The results demonstrated that London showed a minor reduction in accidents of RAR (Relative accident rate) in the radar-based approach (Jaroszweski and McNamara, 2014).


**Road Disasters Caused by Traffic Density**
For transport policy makers it is often imperative to make sure that congestion and road accidents have least impact on cost of transport (Wang, Quddus and Ison, 2009). It was studied that relation between traffic density and road accidents were not possible but maybe inversely related as the potential dilemma for government or policymakers. It was found that Traffic accidents may be increased due to increased traffic volume which leads to high traffic congestion; however, those accidents may be less severe. To count data several statical methods like cleansing and analysis were used. "Data were investigated and disaggregated into two parts: the peak time period when the traffic flow is high; and the off-peak time period when the traffic flow is low. The peak and off-peak time periods were determined by the average hourly traffic volume as showed" (Wang, Quddus and Ison, 2009).

 ![image.png](./assets/image2.png)


It was found that traffic flow is higher during 6:00 am – 8:00 pm in weekdays and 9:00 am – 8:00 pm in weekends these time periods are considered as peak while the rest of the time periods are considered as off-peak. For both peak and off-peak periods, all four model parameters were estimated. A spatial analysis methodology was used to investigate the impact of traffic congestion on road accidents while accounting for the other contributing factors. Accident reports and data on the spatial motorway network are derived from different sources, so mismatches are extremely likely to occur. This means that in a congested environment, the overall external cost of injuries may be lower compared to an uncongested situation (Wang, Quddus and Ison, 2009).

##Research Questions.
_RQ1_: Do developed areas ie Urban areas have higher accident rates compared to rural areas?
_RQ2_: How does weather and light conditions impact the rate of accidents?

# 3. Method
 For the analysis of our report with took a dataset called " 1.6 million UK traffic accidents " from www.kaggle.com which was made by Dave Fisher-Hickey in the year 2017. This dataset is large and contains road traffic information and traffic accident information all the way from 2000 up till 2016. The accident information was subdivided into years and for our analysis, we decided to take up the latest data, 2012 - 2014. The traffic dataset was a huge dataset with information for all the years. We had to first filter out the data according to the years we were working. Then we needed to merge the traffic and the accidents dataset to be able to work on a singular dataset.  Section 3.1 talks about we merged the 2 data files.

## 3.1 Data Cleaning. 

![img3.jpg](./assets/image3.png)

During merging, we also filtered out certain column data to have a concise dataset to work with. For example, " Estimation method " column was brought done to the method of " counted " and the " estimated " methods were filtered out.
 After having merged into one dataset, there was another task. The traffic dataset doesn't give any information about density. And we needed this column for our analysis. So we used the formula for traffic density, 
### _Density = (Number of Vehicles/length of the road)_
Density values were divided into density_levels for understanding and making calculations easy. This was done in the SPSS tool by performing descriptive statistics and finding out Quartiles. Quartiles tell us how our data is distributed, which helped us in dividing the data into levels. Namely, 0-25% data was given density_level = 1 and so on.


## 3.2 Data Analysis.
The analysis started with us first checking the density levels of traffic in the Urban and Rural areas. Fig1 shows that, Urban herein denoted as '1', has a higher density of traffic than rural.
To understand the rate of accidents, we decided to have a bar plot of casualties in the urban and rural areas. Fig2 is a bar plot that shows that Urban areas are highest in terms of having casualties. This means that most of the accidents that have taken place have happened in the Urban areas.
While doing our analysis, we thought it would be interesting to check whether density levels have any effect on the accident rate. Fig3 shows the distribution of casualties in areas with different levels of density.

![img3.jpg](./assets/image4.jpeg)

*Fig1*
![img1.jpg](./assets/image5.jpeg)

*Fig2*
![img7.jpg](./assets/image6.jpeg)

*Fig3*
**************

RQ2 requires us to look at the factors that affect the rate of accidents. In an attempt to answer these questions we have plotted the following bar graphs that represent Weather and Light Conditions.

![img6.jpg](./assets/image7.jpeg)

*Fig4*



# Results
Analysis that resulted in Fig2 clearly shows that the Urban areas experience a higher level of accidents which we can conclude by the following statical data in Fig5. The first row of Fig5 shows that Urban areas have the highest number of casualties and the total casualties is a clear indicator for the overall casualties in the Urban area.

![img5.jpg](./assets/image8.jpeg)

*Fig5*

A statistical analysis of the dataset to determine what kind of weather and light conditions result in a higher number of accidents resulted in the following statistical data.
![img8.jpg](./assets/image9.jpeg)

*Fig6*

Fig4 shows that most of the accidents recorded in the dataset under the ' Weather Conditions' column are due to the condition of ' Fine without high winds '. 85% of the accidents that took place in the year 2012 to 2014 have happened under clear climate conditions.
Similarly, when we look at the light_conditions table, of the Fig6, 71.2% of the occurrences have happened in broad daylight with complete visibility. 
Section 4 discusses the implications of our results and contains the answers to the Research questions we posed in section 2.

# Implications and Discussion
Fig7 shows a Chi-Square test was performed to check the significance of our results. Significance is a test to check whether the results we've obtained in our analysis is a random occurence. The value we've obtained is 0.000 which means that our data is statistically significant ( a good value of p is p <= 0.05).This means we can reject the Null hypothesis. This implies that Urban areas have higher accident rates than rural areas.

![img9.jpg](./assets/image10.jpeg) 

*Fig7*


There are no in-depth details of accidents occurred in the dataset which could have been very helpful to identify and analyse the actual cause of accidents. Although the provided in data is still very powerful which resulted in many of the observations for our report. Some more important arguments could have been made through this dataset like the likelihood of accidents with respect to the speed limit increases or decreases. 

# Conclusions
  Our research demonstrates, Traffic accidents in an urban and rural area and how weather and light conditions affect the rate of accidents in London.
The analysis done reveals that there are more accidents occurring in urban areas. This could be construed as a result of higher traffic density. Further analysis of the dataset to look for an answer on what kind of conditions affect accident rates. Our analysis revealed that in the year 2012 to 2014 the majority of accidents caused were due to dry climates in broad daylight as opposed to rainy seasons or no lights.




# Reference

[1].	Hambly, D. A. J. M. B. a. F. C., 2013. Projected implications of climate change for road safety in Greater Vancouver, Canada. Climatic Change, pp. 116(3-4).

[2].	Edwards, J. (1996). Weather-related road accidents in England and Wales: a spatial analysis. Journal of Transport Geography, 4(3), pp.201-212.

[3].	Edwards, J. (1998). The Relationship Between Road Accident Severity and Recorded Weather. Journal of Safety Research, 29(4), pp.249-262.

[4].	Jaroszweski, D. and McNamara, T. (2014). The influence of rainfall on road accidents in urban areas: A weather radar approach. Travel Behaviour and Society, 1(1), pp.15-21.
[5].	Broughton, J. (1991). Forecasting road accident casualties in Great Britain. Accident Analysis & Prevention, 23(5), pp.353-362.

[6].	Parry, I. and Bento, A. (2002). Estimating the Welfare Effect of Congestion Taxes: The Critical Importance of Other Distortions within the Transport System. Journal of Urban Economics, 51(2), pp.339-365.

[7].	Wang, C., Quddus, M. and Ison, S. (2009). Impact of traffic congestion on road accidents: A spatial analysis of the M25 motorway in England. Accident Analysis & Prevention, [online] 41(4), pp.798-808. 
Available at: https://www.sciencedirect.com/science/article/pii/S0
001457509000797

[8].	Green, C., Heywood, J. and Navarro, M. (2016). Traffic accidents and the London congestion charge. Journal of Public Economics, 133, pp.11-22.

[9].	Andersson, A. and Chapman, L. (2011). The impact of climate change on winter road maintenance and traffic accidents in West Midlands, UK. Accident Analysis & Prevention, 43(1), pp.284-289.

[10].	Vanlaar, W. and Yannis, G. (2006). Perception of road accident causes. Accident Analysis & Prevention, 38(1), pp.155-161.












# Appendix

Bitbucket Repo: 

_https://bitbucket.org/hamzaniazi91/ukaccidents_team_
research/src/master/_

Trello Board:  

_https://trello.com/b/AKPLqTN6/teamresearchpart-a/_

Azure Team:  

_https://dev.azure.com/hn19aal/Team%20Research%20Group%2019/_

